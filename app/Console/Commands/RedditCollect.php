<?php

namespace App\Console\Commands;

use App\Models\RedditNewsItem;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RedditCollect extends Command
{
    const BASE_URl = 'https://www.reddit.com/r';
    const AVAILABLE_SECTIONS = [
        'hot',
        'new',
        'top',
        'rising'
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reddit:collect
                            {subreddit : Name of the required subreddit}
                            {limit? : Limiting the number of news}
                            {--section= : Available reddit sections: "hot", "new", "top", "rising"}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collect news of the specified subreddit.';

    /**
     * @var string|null
     */
    private $subreddit;

    /**
     * @var int|null
     */
    private $limit;

    /**
     * @var string|null
     */
    private $section;

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $news;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->setArgs();
        $this->collectData();

        $this->info('News successfully collected');
    }

    private function setArgs()
    {
        $validated = $this->validate();
        $this->subreddit = $validated['subreddit'];
        $this->section = $validated['section'];
        $this->limit = $validated['limit'];
        $this->url = self::BASE_URl . "/{$this->subreddit}/{$this->section}" .
            ".json?limit={$this->limit}";
    }

    private function validate()
    {
        $validator = Validator::make([
            'subreddit' => $this->argument('subreddit'),
            'limit' => $this->argument('limit'),
            'section' => $this->option('section'),
        ], [
            'subreddit' => ['string'],
            'limit' => ['nullable', 'numeric'],
            'section' => ['nullable', Rule::in(self::AVAILABLE_SECTIONS)],
        ]);

        if ($validator->fails()) {
            $this->info('Error(s) occurred while performing the command:');
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }

            exit;
        }

        return $validator->validated();
    }

    private function collectData()
    {
        $client = Http::get($this->url);
        $response = $client->json();
        $this->parseNewsFrom($response)->persistDB()->persistStorage();
    }

    private function parseNewsFrom(array $response)
    {
        $this->limit = $this->limit ?: $response['data']['dist'];
        $this->news = array_map(fn($item) => $item['data'],
            // In several url received news amount doesn't matches
            // with specified limit. So we slice array manually.
            (int)$response['data']['dist'] != (int)$this->limit ?
                array_slice(
                    $response['data']['children'],
                    0,
                    -($response['data']['dist'] - $this->limit)
                ) : $response['data']['children']
        );

        return $this;
    }

    private function persistDB()
    {
        foreach ($this->news as $newsItem) {
            RedditNewsItem::updateOrCreate([
                'title' => $newsItem['title'],
                'selftext' => $newsItem['selftext'],
                'url' => $newsItem['url'],
                'permalink' => $newsItem['permalink'],
                'subreddit' => $this->subreddit,
            ]);
        }

        return $this;
    }

    private function persistStorage()
    {
        Storage::disk('public')->put('reddit_news.json', RedditNewsItem::all()->toJson());

        return $this;
    }
}
