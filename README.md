RU
# News Collector
  
## Требования  
  
- PHP 7.4  
- MySQL 5.6+  
- NodeJS 11+  
  
## Установка  
  
#### 1) Склонируйте проект Запустите команду `git clone git@bitbucket.org:timur_alchibekov/news-collector.git` в папке со всеми проектами (убедитесь, что добавили свой SSH ключ в bitbucket)  
  
#### 2) Установите зависимости composer  
Запустите команду `composer install`  
  
#### 3) Создайте файл с переменными окружения (.env)  
Скопируйте `.env.example` в `.env`  
  
#### 4) Сгенерируйте ключ приложения  
Запустите команду `php artisan key:generate`  
  
#### 5) Создайте базу данных  
  
#### 6) Заполните переменные окружения в файле `.env`  
- Укажите в `DB_DATABASE`, `DB_USERNAME` и `DB_PASSWORD` соответствующие значения  
- Укажите в `APP_URL` URL приложения  
  
#### 7) Запустите миграции (для создания таблиц в базе данных)  
Запутите команду `php artisan migrate`  
  
#### 8) Установите Front End зависимости  
Запустите команду `yarn install`  
  
#### 9) Запустите компиляцию  
Запустите команду `yarn dev`  

#### 10) Создайте Cron Job на вашем сервере. 
Если ваш сервер на Linux, в консоли введите `crontab -e`, затем добавьте строчку `* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1` 

#### 11) Создайте публичную ссылку для storage командой `php artisan storage:link`

#### 12) Наслаждайтесь!

EN
# News Collector
  
## Requirements
  
- PHP 7.4
- MySQL 5.6+
- NodeJS 11+
  
## Installation
  
#### 1) Clone the project Run command `git clone git@bitbucket.org: timur_alchibekov / news-collector.git` in all projects folder (make sure to add your SSH key to bitbucket)
  
#### 2) Install composer dependencies
Run the command `composer install`
  
#### 3) Create a file with environment variables (.env)
Copy `.env.example` to` .env`
  
#### 4) Generate Application Key
Run command `php artisan key: generate`
  
#### 5) Create a database
  
#### 6) Fill in the environment variables in the `.env` file
- Specify `DB_DATABASE`,` DB_USERNAME` and `DB_PASSWORD` corresponding values
- Specify in ʻAPP_URL` the URL of the application
  
#### 7) Run migrations (to create tables in the database)
Confuse the `php artisan migrate` command
  
#### 8) Install Front End dependencies
Run the command `yarn install`
  
#### 9) Start compilation
Run the command `yarn dev`

#### 10) Create a Cron Job on your server.
If your server is on Linux, in the console enter `crontab -e`, then add the line `* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1`

#### 11) Create a public link for storage with the command `php artisan storage: link`

#### 12) Enjoy!
